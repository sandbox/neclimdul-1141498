<?php

/**
 * Define our plugin definition.
 */
$plugin = array(
  // This is a 'page' task and will fall under the page admin UI
  'task type' => 'page',

  'title' => t('404 error page'),
  'admin title' => t('404 error page'),
  'admin description' => t('When enabled this takes over error pages and does magical things.'),
  'admin path' => '404.html',

  // Menu hooks so that we can alter the node/%node menu entry to point to us.
  'hook menu' => 'page_manager_errors_error_page_404_menu',

  // This is task uses 'context' handlers and must implement these to give the
  // handler data it needs.
  'handler type' => 'context',

  // Allow this to be enabled or disabled:
  'disabled' => variable_get('page_manager_errors_error_page_404_disabled', TRUE),
  'enable callback' => 'page_manager_errors_error_page_404_enable',
);

/**
 * Plugin hook_menu callback.
 */
function page_manager_errors_error_page_404_menu(&$items, $task) {
  if (!variable_get('page_manager_errors_error_page_404_disabled', TRUE)) {
    $items['404.html'] = array(
      'title' => '404 Not Found',
      'page callback' => 'page_manager_errors_error_page_404_page',
      'page arguments' => array(),
      // If everyone can't access it, its not a very good error page.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
      'file path' => $task['path'],
      'file' => $task['file'],
    );
  }
}

/**
 * Page callback for our 404 error page.
 */
function page_manager_errors_error_page_404_page() {
    // Load my task plugin
  $task = page_manager_get_task('error_page_404');

  ctools_include('context');
  ctools_include('context-task-handler');
  // TODO Build additional contexts.

  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
    return $output;
  }

  $function = '';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('error_page_404')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }

  if ($function && function_exists($function)) {
    return $function;
  }

  return '404 Not Found';
}


/**
 * Callback to enable/disable the page from the UI.
 */
function page_manager_errors_error_page_404_enable($cache, $status) {
  variable_set('page_manager_errors_error_page_404_disabled', $status);
}
